export type RestaurantCategory = {
    _id: string,
    name: string,
    created_at: string
}

export type RestaurantCategoryInput = {
    name: string
}
import { Meteor } from 'meteor/meteor'
import React from 'react'
import RestaurantCategoryForm from '../components/forms/RestaurantCategoryForm'
import RestaurantCategoriesDisplayer from '../components/RestaurantCategoriesDisplayer'

const LandingPage = () => {

    return(
        <>
            <h1>LANDING PAGE</h1>
            <RestaurantCategoriesDisplayer />

            <RestaurantCategoryForm />
        </>
    )
}

export default LandingPage